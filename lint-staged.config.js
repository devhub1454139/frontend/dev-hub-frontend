module.exports = {
  '**/*.{js,ts,tsx}': [
    'eslint --fix',
    'prettier --config ./.prettierrc --write'
  ],
  '**/*.{scss,md,json}': ['prettier --config ./.prettierrc --write'],
  '**/*.{scss}': ['stylelint --fix']
};
