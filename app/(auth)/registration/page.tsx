import { Metadata } from 'next';
import Link from 'next/link';
import { RegisterFormByCredentials } from '@/features/auth/registration/by-credentials';
import { getRouteSignIn } from '@/shared/const/router';

export const metadata: Metadata = {
  title: 'DevHub | Registration'
};

const RegistrationPage = () => (
  <div className="flex flex-col items-start max-w-[480px] w-full">
    <h1 className="text-3xl font-bold text-center mb-12 w-full">Регистация</h1>

    <RegisterFormByCredentials />

    <span className="inline-flex gap-2 justify-center w-full text-lg mt-6">
      <span className="text-base">Есть аккаунт?</span>
      <Link
        href={getRouteSignIn()}
        className="text-base font-bold hover:underline"
      >
        Войти
      </Link>
    </span>
  </div>
);

export default RegistrationPage;
