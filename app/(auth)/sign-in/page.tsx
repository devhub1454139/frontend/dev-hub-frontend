import Link from 'next/link';
import Image from 'next/image';
import clappingHandsEmoji from '@/public/icons/emoji/clapping-hands.png';
import { Metadata } from 'next';
import { SignInFormByCredentials } from '@/features/auth/sign-in/by-credentials';
import { getRouteRegistration } from '@/shared/const/router';

export const metadata: Metadata = {
  title: 'DevHub | SignIn'
};

const SignIn = () => (
  <div className="flex flex-col items-start max-w-[480px] w-full">
    <span className="flex gap-2 items-center mb-5 md:mb-10">
      <h1 className="text-2xl md:text-3xl font-bold">
        Добро <span className="text-blue-600">Пожаловать</span>
      </h1>
      <Image
        src={clappingHandsEmoji}
        alt="clapping-hands"
        width={36}
        height={36}
      />
    </span>

    <SignInFormByCredentials />

    <span className="inline-flex gap-2 justify-center w-full text-lg mt-6">
      <span className="text-base">Нет аккаунта?</span>
      <Link
        href={getRouteRegistration()}
        className="text-base font-bold hover:underline"
      >
        Создать
      </Link>
    </span>
  </div>
);

export default SignIn;
