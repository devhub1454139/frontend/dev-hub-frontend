'use client';

import { Spinner } from '@nextui-org/react';

const Loading = () => <Spinner />;

export default Loading;
