import { ReactNode } from 'react';

const AuthLayout = ({ children }: { children: ReactNode }) => (
  <section className="flex flex-col justify-center items-center w-screen h-[100svh] container mx-auto px-4">
    {children}
  </section>
);

export default AuthLayout;
