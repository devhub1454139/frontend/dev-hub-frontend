'use client';

import { SessionProvider } from 'next-auth/react';
import { NextUIProvider } from '@nextui-org/react';
import { ReactNode } from 'react';

export const AppProviders = ({ children }: { children: ReactNode }) => (
  <SessionProvider>
    <NextUIProvider>{children}</NextUIProvider>
  </SessionProvider>
);
