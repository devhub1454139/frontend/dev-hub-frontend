import '@/app/_styles/globals.scss';
import { Inter } from 'next/font/google';
import { ReactNode } from 'react';
import { AppProviders } from '@/app/_providers/AppProviders';

const inter = Inter({ subsets: ['latin'] });

const RootLayout = ({ children }: { children: ReactNode }) => (
  <html lang="en" className="dark">
    <body className={inter.className}>
      <AppProviders>{children}</AppProviders>
    </body>
  </html>
);

export default RootLayout;
