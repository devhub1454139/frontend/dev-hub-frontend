import { ArticleModel } from '@/entities/article';
import { Metadata } from 'next';
import { ArticleList } from '@/widgets/article/article-list';

export const metadata: Metadata = {
  title: 'DevHub | Flame'
};

const articles: ArticleModel[] = [
  {
    id: 1,
    title: 'Заголовок',
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    img: 'https://habrastorage.org/r/w1560/getpro/habr/upload_files/5e7/c18/d82/5e7c18d825e0af0b20c33378fef33a55.png',
    likes: 400,
    dislikes: 300
  },
  {
    id: 2,
    title: 'Заголовок',
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    likes: 400,
    dislikes: 300
  },
  {
    id: 3,
    title: 'Заголовок',
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    likes: 400,
    dislikes: 300
  },
  {
    id: 4,
    title: 'Заголовок',
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    img: 'https://habrastorage.org/r/w1560/getpro/habr/upload_files/c91/9c1/5cb/c919c15cb47970429261375b022dec0c.png',
    likes: 400,
    dislikes: 300
  }
];

const Home = () => (
  <main>
    <div className="container mx-auto max-w-[1000px]">
      <ArticleList articles={articles} />
    </div>
  </main>
);

export default Home;
