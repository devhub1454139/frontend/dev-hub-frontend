'use client';

import { Button } from '@nextui-org/react';
import notFoundLottieAnimation from '@/public/lotties/404.json';
import Lottie from 'lottie-react';
import Link from 'next/link';

const NotFoundPage = () => (
  <section className="flex flex-col justify-center items-center w-screen h-[100svh] container mx-auto px-4">
    <Lottie
      className="max-h-[360px] bg-transparent"
      animationData={notFoundLottieAnimation}
      loop
    />
    <div className="flex flex-col items-center max-w-[500px] text-center mb-5 md:mb-10">
      <h1 className="text-2xl md:text-4xl font-bold mb-2 md:mb-3">
        Страница не найдена
      </h1>
      <p className="md:text-xl">
        Мы здесь пока ничего не написали, но у нас ещё много других интересных
        страниц.
      </p>
    </div>
    <Button as={Link} href="/" radius="lg" size="lg" color="primary">
      Вернуться домой
    </Button>
  </section>
);

export default NotFoundPage;
