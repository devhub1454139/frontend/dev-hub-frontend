import { AuthOptions } from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';
import { getRouteSignIn } from '@/shared/const/router';
import {
  SignInByCredentialsService,
  SignInMapper
} from '@/features/auth/sign-in/by-credentials';

export const authConfig: AuthOptions = {
  providers: [
    CredentialsProvider({
      id: 'credentials',
      name: 'Credentials',
      credentials: {
        login: { label: 'Login', type: 'text' },
        password: { label: 'Password', type: 'password' }
      },
      async authorize(credentials) {
        if (!credentials) {
          throw new Error('No credentials.');
        }
        const dto = SignInMapper.toDto(credentials);
        await SignInByCredentialsService.signIn(dto);
        return null;
      }
    })
  ],
  pages: {
    signIn: getRouteSignIn()
  }
};
