'use client';

import { useEffect } from 'react';
import { Button } from '@nextui-org/react';
import errorLottieAnimation from '@/public/lotties/error.json';
import Lottie from 'lottie-react';

const Error = ({ error, reset }: { error: Error; reset: () => void }) => {
  useEffect(() => {
    // TODO: add error reporting service
    console.error(error);
  }, [error]);

  return (
    <section className="flex flex-col justify-center items-center w-screen h-[100svh] container mx-auto px-4">
      <Lottie
        className="max-h-[240px] bg-transparent mb-4"
        animationData={errorLottieAnimation}
        loop
      />
      <div className="flex flex-col items-center max-w-[360px] md:max-w-[500px] text-center mb-5">
        <h1 className="text-2xl md:text-4xl font-bold mb-2 md:mb-3">
          Что-то пошло не так!
        </h1>
        <p className="md:text-xl">
          Произошла неизвестная ошибка, которую мы не факт, что исправим
        </p>
      </div>
      <Button onClick={() => reset()} radius="lg" size="lg" color="primary">
        Попробовать снова
      </Button>
    </section>
  );
};

export default Error;
