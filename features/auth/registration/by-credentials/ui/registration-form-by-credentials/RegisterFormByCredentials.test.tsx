import { render } from '@testing-library/react';
import { RegisterFormByCredentials } from './RegisterFormByCredentials';

describe('RegisterFormByCredentials', () => {
  it('snapshot', () => {
    const { container } = render(<RegisterFormByCredentials />);
    expect(container).toMatchSnapshot();
  });
});
