'use client';

import { Button, Input } from '@nextui-org/react';
import { Eye, EyeOff, Mail, User2Icon } from 'lucide-react';
import { useRegistrationByCredentials } from '../../model';

export const RegisterFormByCredentials = () => {
  const {
    registerEmail,
    registerPassword,
    registerLogin,
    togglePasswordVisibility,
    passwordVisible,
    errors,
    handleSubmit
  } = useRegistrationByCredentials();

  const getPasswordIcon = (
    <button
      className="focus:outline-none"
      type="button"
      onClick={togglePasswordVisibility}
    >
      {passwordVisible ? (
        <EyeOff className="text-2xl text-default-400 pointer-events-none" />
      ) : (
        <Eye className="text-2xl text-default-400 pointer-events-none" />
      )}
    </button>
  );

  const getMailIcon = (
    <Mail className="text-2xl text-default-400 pointer-events-none flex-shrink-0" />
  );

  const getUserIcon = <User2Icon className="text-default-400 text-2xl" />;

  return (
    <form onSubmit={handleSubmit} className="flex flex-col gap-4 w-full">
      <Input
        type="text"
        label="Логин"
        variant="flat"
        endContent={getUserIcon}
        autoComplete="username"
        isInvalid={Boolean(errors.login)}
        errorMessage={errors.login?.message}
        {...registerLogin}
      />
      <Input
        type="email"
        label="Почта"
        variant="flat"
        endContent={getMailIcon}
        autoComplete="email"
        isInvalid={Boolean(errors.email)}
        errorMessage={errors.email?.message}
        {...registerEmail}
      />
      <Input
        label="Пароль"
        variant="flat"
        autoComplete="new-password"
        endContent={getPasswordIcon}
        type={passwordVisible ? 'text' : 'password'}
        className="mb-3"
        isInvalid={Boolean(errors.password)}
        errorMessage={errors.password?.message}
        {...registerPassword}
      />
      <Button size="lg" type="submit" color="primary">
        Зарегистрироваться
      </Button>
    </form>
  );
};
