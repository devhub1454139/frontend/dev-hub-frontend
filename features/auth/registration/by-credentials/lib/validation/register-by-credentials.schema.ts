import * as yup from 'yup';

export const registrationByCredentialsSchema = yup.object({
  email: yup
    .string()
    .email('Email must be a valid email')
    .required('Email is a required field'),
  login: yup
    .string()
    .min(3, 'Login must be at least 3 characters')
    .required('Login is a required field'),
  password: yup
    .string()
    .min(5, 'Password must be at least 5 characters')
    .max(12, 'Password must be at most 12 characters')
    .required('Password is a required field')
});
