import { RegistrationMapper } from '../registration-mapper.util';

describe('RegistrationMapper', () => {
  it('maps model to dto correctly', () => {
    const model = {
      login: 'testuser',
      email: 'test@example.com',
      password: 'testpassword'
    };

    const result = RegistrationMapper.toDto(model);

    expect(result).toEqual({
      login: 'testuser',
      email: 'test@example.com',
      password: 'testpassword'
    });
  });

  it('maps dto to model correctly', () => {
    const dto = {
      login: 'testuser',
      email: 'test@example.com',
      password: 'testpassword'
    };

    const result = RegistrationMapper.toModel(dto);

    expect(result).toEqual({
      login: 'testuser',
      email: 'test@example.com',
      password: 'testpassword'
    });
  });
});
