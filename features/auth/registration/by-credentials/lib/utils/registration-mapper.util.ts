import { RegistrationByCredentialsDto } from '../../model/dto';
import { RegistrationModelByCredentials } from '../../model/types';

export class RegistrationMapper {
  static toDto(
    model: RegistrationModelByCredentials
  ): RegistrationByCredentialsDto {
    return {
      login: model.login,
      email: model.email,
      password: model.password
    };
  }

  static toModel(
    dto: RegistrationByCredentialsDto
  ): RegistrationModelByCredentials {
    return {
      login: dto.login,
      email: dto.email,
      password: dto.password
    };
  }
}
