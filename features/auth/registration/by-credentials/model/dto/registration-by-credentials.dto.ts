export interface RegistrationByCredentialsDto {
  login: string;
  email: string;
  password: string;
}
