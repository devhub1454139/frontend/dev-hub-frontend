import { RegistrationByCredentialsDto } from '../../dto';

export class RegisterByCredentialsService {
  static registration(
    params: RegistrationByCredentialsDto
  ): Promise<unknown> | null {
    return fetch(`http://localhost:5000/api/auth/registration`, {
      method: 'POST',
      mode: 'cors',
      credentials: 'include',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(params)
    });
  }
}
