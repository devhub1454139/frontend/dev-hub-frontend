import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { RegisterByCredentialsService } from '../services';
import { RegistrationModelByCredentials } from '../types';
import { registrationByCredentialsSchema, RegistrationMapper } from '../../lib';

export const useRegistrationByCredentials = () => {
  const [passwordVisible, setPasswordVisible] = useState(false);

  const {
    handleSubmit,
    register,
    formState: { errors }
  } = useForm<RegistrationModelByCredentials>({
    mode: 'onChange',
    resolver: yupResolver(registrationByCredentialsSchema)
  });

  const togglePasswordVisibility = () => setPasswordVisible(prev => !prev);

  const onSubmit = async (data: RegistrationModelByCredentials) => {
    const dto = RegistrationMapper.toDto(data);
    await RegisterByCredentialsService.registration(dto);
  };

  return {
    togglePasswordVisibility,
    passwordVisible,
    errors,
    registerEmail: register('email'),
    registerPassword: register('password'),
    registerLogin: register('login'),
    handleSubmit: handleSubmit(onSubmit)
  };
};
