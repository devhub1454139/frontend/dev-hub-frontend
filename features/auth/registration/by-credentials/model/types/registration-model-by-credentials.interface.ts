export interface RegistrationModelByCredentials {
  login: string;
  email: string;
  password: string;
}
