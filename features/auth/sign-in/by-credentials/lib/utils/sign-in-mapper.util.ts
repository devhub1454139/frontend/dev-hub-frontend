import { SignInByCredentialsDto } from '../../model/dto';
import { SignInModelByCredentials } from '../../model/types';

export class SignInMapper {
  static toDto(model: SignInModelByCredentials): SignInByCredentialsDto {
    return {
      username: model.login,
      password: model.password
    };
  }

  static toModel(dto: SignInByCredentialsDto): SignInModelByCredentials {
    return {
      login: dto.username,
      password: dto.password
    };
  }
}
