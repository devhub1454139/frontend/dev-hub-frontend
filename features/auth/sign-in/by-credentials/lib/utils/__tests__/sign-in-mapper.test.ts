import { SignInMapper } from '../sign-in-mapper.util';

describe('SignInMapper', () => {
  it('maps model to dto correctly', () => {
    const model = {
      login: 'testuser',
      password: 'testpassword'
    };

    const result = SignInMapper.toDto(model);

    expect(result).toEqual({
      username: 'testuser',
      password: 'testpassword'
    });
  });

  it('maps dto to model correctly', () => {
    const dto = {
      username: 'testuser',
      password: 'testpassword'
    };

    const result = SignInMapper.toModel(dto);

    expect(result).toEqual({
      login: 'testuser',
      password: 'testpassword'
    });
  });
});
