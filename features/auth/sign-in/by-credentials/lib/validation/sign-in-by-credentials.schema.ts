import * as yup from 'yup';

export const signInByCredentialsSchema = yup.object({
  login: yup
    .string()
    .min(3, 'login must be at least 5 characters')
    .required('Login is a required field'),
  password: yup
    .string()
    .min(5, 'Password must be at least 5 characters')
    .max(12, 'Password must be at most 12 characters')
    .required('Password is a required field')
});
