import { render } from '@testing-library/react';
import { SignInFormByCredentials } from './SignInFormByCredentials';

describe('SignInFormByCredentials', () => {
  it('snapshot', () => {
    const { container } = render(<SignInFormByCredentials />);
    expect(container).toMatchSnapshot();
  });
});
