'use client';

import { Button, Input } from '@nextui-org/react';
import { Eye, EyeOff, User2Icon } from 'lucide-react';
import { useSignInByCredentials } from '../../model';

export const SignInFormByCredentials = () => {
  const {
    handleSubmit,
    errors,
    registerLogin,
    registerPassword,
    togglePasswordVisibility,
    passwordVisible
  } = useSignInByCredentials();

  const getPasswordIcon = (
    <button
      className="focus:outline-none"
      type="button"
      onClick={togglePasswordVisibility}
    >
      {passwordVisible ? (
        <EyeOff className="text-2xl text-default-400 pointer-events-none" />
      ) : (
        <Eye className="text-2xl text-default-400 pointer-events-none" />
      )}
    </button>
  );

  const getUserIcon = <User2Icon className="text-default-400 text-2xl" />;

  return (
    <form onSubmit={handleSubmit} className="flex flex-col gap-4 w-full">
      <Input
        type="text"
        label="Логин"
        variant="flat"
        endContent={getUserIcon}
        autoComplete="username"
        isInvalid={Boolean(errors.login)}
        errorMessage={errors.login?.message}
        {...registerLogin}
      />
      <Input
        label="Пароль"
        variant="flat"
        autoComplete="current-password"
        endContent={getPasswordIcon}
        type={passwordVisible ? 'text' : 'password'}
        className="mb-3"
        isInvalid={Boolean(errors.password)}
        errorMessage={errors.password?.message}
        {...registerPassword}
      />
      <Button size="lg" type="submit" color="primary">
        Войти
      </Button>
    </form>
  );
};
