'use client';

import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { signIn } from 'next-auth/react';
import { useRouter } from 'next/navigation';
import { getRouteMain } from '@/shared/const/router';
import { SignInModelByCredentials } from '../types';
import { signInByCredentialsSchema } from '../../lib';

export const useSignInByCredentials = () => {
  const [passwordVisible, setPasswordVisible] = useState(false);
  const router = useRouter();

  const {
    handleSubmit,
    register,
    formState: { errors }
  } = useForm<SignInModelByCredentials>({
    mode: 'onChange',
    resolver: yupResolver(signInByCredentialsSchema)
  });

  const togglePasswordVisibility = () => setPasswordVisible(prev => !prev);

  const onSubmit = async (data: SignInModelByCredentials) => {
    const result = await signIn('credentials', {
      login: data.login,
      password: data.password,
      redirect: false
    });
    if (result?.ok) {
      router.push(getRouteMain());
    }
  };

  return {
    handleSubmit: handleSubmit(onSubmit),
    registerLogin: register('login'),
    registerPassword: register('password'),
    errors,
    togglePasswordVisibility,
    passwordVisible
  };
};
