export interface SignInByCredentialsDto {
  username: string;
  password: string;
}
