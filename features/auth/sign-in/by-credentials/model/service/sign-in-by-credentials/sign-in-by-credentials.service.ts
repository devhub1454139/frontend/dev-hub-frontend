import { SignInByCredentialsDto } from '../../dto';

export class SignInByCredentialsService {
  static signIn(params: SignInByCredentialsDto): Promise<unknown> {
    return fetch(`http://localhost:5000/api/auth/login`, {
      method: 'POST',
      mode: 'cors',
      credentials: 'include',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(params)
    });
  }
}
