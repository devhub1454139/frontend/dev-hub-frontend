export interface SignInModelByCredentials {
  login: string;
  password: string;
}
