import { Bookmark } from 'lucide-react';
import { Button } from '@nextui-org/react';

interface Props {
  count: number;
}

export const AddToBookmarkButton = ({ count }: Props) => (
  <Button
    radius="full"
    variant="light"
    startContent={<Bookmark className="w-[20px] h-[20px]" />}
  >
    <span className="text-sm">{count}</span>
  </Button>
);
