import { Button } from '@nextui-org/react';
import { ThumbsDown } from 'lucide-react';

interface Props {
  count: number;
}

export const DislikeButton = ({ count }: Props) => (
  <Button
    radius="full"
    variant="light"
    startContent={<ThumbsDown className="w-[20px] h-[20px]" />}
  >
    <span className="text-sm">{count}</span>
  </Button>
);
