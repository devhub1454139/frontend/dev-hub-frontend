import { Button } from '@nextui-org/react';
import { ThumbsUp } from 'lucide-react';

export interface LikeButtonProps {
  count: number;
}

export const LikeButton = ({ count }: LikeButtonProps) => (
  <Button
    radius="full"
    variant="light"
    startContent={<ThumbsUp className="w-[20px] h-[20px]" />}
  >
    <span className="text-sm">{count}</span>
  </Button>
);
