'use client';

import {
  Listbox,
  ListboxItem,
  Popover,
  PopoverContent,
  PopoverTrigger
} from '@nextui-org/react';
import { ReactNode } from 'react';
import { useAvatarDropdown } from '../../model/hooks';
import { ListBoxWrapper } from './list-box-wrapper/ListBoxWrapper';

interface Props {
  UserProfileSlot: ReactNode;
}

export const AvatarDropdown = ({ UserProfileSlot }: Props) => {
  const { dropdownItems } = useAvatarDropdown();

  return (
    <Popover showArrow placement="bottom" backdrop="transparent">
      <PopoverTrigger>{UserProfileSlot}</PopoverTrigger>
      <PopoverContent className="p-0">
        <ListBoxWrapper>
          <Listbox variant="faded" aria-label="user listbox">
            {dropdownItems.map(({ label, Icon }) => (
              <ListboxItem key={label} startContent={<Icon />}>
                {label}
              </ListboxItem>
            ))}
          </Listbox>
        </ListBoxWrapper>
      </PopoverContent>
    </Popover>
  );
};
