import { ComponentType } from 'react';

export interface ListItem {
  label: string;
  Icon: ComponentType;
  href?: string;
  action?: () => void;
}
