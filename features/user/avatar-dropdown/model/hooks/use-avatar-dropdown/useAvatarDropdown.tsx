import { Cog, DoorOpen, User2 } from 'lucide-react';
import { ListItem } from '../../types';

export const useAvatarDropdown = () => {
  const dropdownItems: ListItem[] = [
    {
      label: 'Профиль',
      Icon: User2
    },
    {
      label: 'Настройки',
      Icon: Cog
    },
    {
      label: 'Выйти',
      Icon: DoorOpen
    }
  ];

  return { dropdownItems };
};
