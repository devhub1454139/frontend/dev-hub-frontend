'use client';

import { Avatar, Image } from '@nextui-org/react';
import NextImage from 'next/image';
import { Tag } from '@/shared/ui/tag';
import { Eye } from 'lucide-react';
import { ReactNode } from 'react';
import { ArticleModel } from '../../model/types';

interface Props {
  article: ArticleModel;
  likeButtonSlot?: ReactNode;
  dislikeButtonSlot?: ReactNode;
  addToBookmarkButtonSlot?: ReactNode;
}

export const ArticleCard = ({
  article,
  likeButtonSlot,
  dislikeButtonSlot,
  addToBookmarkButtonSlot
}: Props) => (
  <article className="bg-white dark:bg-gray-900 dark:text-white w-full flex flex-col gap-4 rounded-3xl shadow-md md:shadow-xl p-4">
    <header className="flex justify-between items-center">
      <div className="flex items-center gap-4">
        <Avatar
          src="https://i.pravatar.cc/150?u=a042581f4e29026024d"
          size="md"
        />
        <span className="text-sm">9 часов назад</span>
      </div>
      <div className="flex items-center gap-2">
        <Tag className="bg-purple-600">Kotlin</Tag>
        <Tag className="bg-green-700">Compose</Tag>
      </div>
    </header>
    <h4 className="text-lg md:text-xl font-bold">{article.title}</h4>
    <p className="text-sm md:text-md">{article.description}</p>
    {article.img && (
      <div className="relative w-full h-[200px] md:h-[300px] lg:h-[400px]">
        <Image
          as={NextImage}
          fill
          src={article.img}
          removeWrapper
          priority
          quality={75}
          className="object-cover"
          alt="image"
        />
      </div>
    )}
    <footer className="flex justify-between items-center">
      <div className="flex items-center gap-1">
        {likeButtonSlot && likeButtonSlot}
        {dislikeButtonSlot && dislikeButtonSlot}
        {addToBookmarkButtonSlot && addToBookmarkButtonSlot}
      </div>
      <div className="flex items-center gap-3">
        <Eye className="w-[20px] h-[20px]" />
        <span className="text-sm">22</span>
      </div>
    </footer>
  </article>
);
