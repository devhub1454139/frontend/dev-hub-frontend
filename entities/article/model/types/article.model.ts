export interface ArticleModel {
  id: number;
  title: string;
  description?: string;
  img?: string;
  likes: number;
  dislikes: number;
}
