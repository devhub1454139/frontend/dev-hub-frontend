export enum UserRoles {
  USER = 'user',
  MODERATOR = 'moderator',
  ADMIN = 'admin',
  PROJECT_OWNER = 'project_owner'
}
