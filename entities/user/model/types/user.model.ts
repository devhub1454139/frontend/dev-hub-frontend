import { UniqueId } from '@/shared/types/unique-id.type';
import { UserRoles } from '@/entities/user-role';

export interface UserModel {
  id: UniqueId;
  email: string;
  image?: string;
  role: UserRoles[];
}
