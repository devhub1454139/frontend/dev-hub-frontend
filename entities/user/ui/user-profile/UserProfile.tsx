import { User } from '@nextui-org/user';
import classNames from 'classnames';

interface Props {
  avatarSrc?: string;
  description?: string;
  name: string;
  className?: string;
}
export const UserProfile = ({
  avatarSrc,
  name,
  description,
  className
}: Props) => (
  <User
    name={name}
    className={classNames(className, 'cursor-pointer')}
    description={description}
    avatarProps={{
      src: avatarSrc
    }}
  />
);
