'use client';

import { ArticleCard, ArticleModel } from '@/entities/article';
import { LikeButton } from '@/features/article/like';
import { DislikeButton } from '@/features/article/dislike';
import { AddToBookmarkButton } from '@/features/article/bookmark';

interface Props {
  articles: ArticleModel[];
}
export const ArticleList = ({ articles }: Props) => (
  <div className=" flex flex-col gap-6 px-2 md:px-3">
    {articles.map(article => (
      <ArticleCard
        key={article.id}
        article={article}
        likeButtonSlot={<LikeButton count={article.likes} />}
        dislikeButtonSlot={<DislikeButton count={article.dislikes} />}
        addToBookmarkButtonSlot={<AddToBookmarkButton count={0} />}
      />
    ))}
  </div>
);
