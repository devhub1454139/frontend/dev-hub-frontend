import { ComponentType } from 'react';
import { LucideProps } from 'lucide-react';

export interface MenuItem {
  label: string;
  IconComponent: ComponentType<LucideProps>;
}
