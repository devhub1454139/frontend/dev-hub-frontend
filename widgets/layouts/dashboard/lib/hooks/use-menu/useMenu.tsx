import { Flame, Menu, Scroll, UserPlus, Users2 } from 'lucide-react';
import { MenuItem } from '../../../model/types';

interface UseMenu {
  desktopMenu: MenuItem[];
  mobileNavigation: MenuItem[];
}

export const useMenu = (): UseMenu => {
  const desktopMenu: MenuItem[] = [
    {
      label: 'Горячее',
      IconComponent: Flame
    },
    {
      label: 'Моя лента',
      IconComponent: Scroll
    },
    {
      label: 'Подписки',
      IconComponent: UserPlus
    },
    {
      label: 'О нас',
      IconComponent: Users2
    }
  ];

  const mobileNavigation: MenuItem[] = [
    {
      label: 'Моя лента',
      IconComponent: Scroll
    },
    {
      label: 'Горячее',
      IconComponent: Flame
    },
    {
      label: 'Подписки',
      IconComponent: UserPlus
    },
    {
      label: 'Основное',
      IconComponent: Menu
    }
  ];

  return { desktopMenu, mobileNavigation };
};
