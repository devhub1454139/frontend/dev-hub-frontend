import { PropsWithChildren } from 'react';
import classNames from 'classnames';
import { MobileNavigation } from '../mobile-navigation';
import cls from './dashboard.module.scss';
import { Sidebar } from '../sidebar';
import { Header } from '../header';

interface DashboardProps extends PropsWithChildren {}

export const Dashboard = ({ children }: DashboardProps) => (
  <section className={classNames(cls.dashboard)}>
    <Header />
    <div className={classNames(cls.dashboardWrapper, 'flex-col lg:flex-row')}>
      <Sidebar
        className={classNames(cls.dashboardSidebar, 'hidden lg:block')}
      />
      <section className={cls.dashboardMain}>{children}</section>
      <MobileNavigation />
    </div>
  </section>
);
