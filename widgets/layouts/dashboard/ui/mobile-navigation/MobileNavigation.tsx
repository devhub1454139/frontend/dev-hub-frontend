import { useMenu } from '../../lib/hooks';

export const MobileNavigation = () => {
  const { mobileNavigation } = useMenu();

  return (
    <nav className="flex lg:hidden items-center justify-center gap-4 fixed bottom-0.5 w-full px-4 mb-2 z-20">
      <div className="flex gap-1 p-1 bg-white rounded-2xl w-full border border-solid border-gray-100 shadow-xl dark:bg-gray-950 dark:border-gray-900">
        {mobileNavigation.map(({ label, IconComponent }) => (
          <div
            key={label}
            className="flex gap-4 items-center justify-center p-3 rounded-xl transition text-black flex-1 hover:bg-gray-900 dark:text-white cursor-pointer"
          >
            <IconComponent width={24} height={24} />
          </div>
        ))}
      </div>
    </nav>
  );
};
