import classNames from 'classnames';
import { useMenu } from '../../lib/hooks';
import cls from './sidebar.module.scss';
import { MenuList } from '../menu/menu-list';

interface Props {
  className?: string;
}

export const Sidebar = ({ className }: Props) => {
  const { desktopMenu } = useMenu();

  return (
    <aside className={classNames(cls.sidebar, className)}>
      <MenuList items={desktopMenu} />
    </aside>
  );
};
