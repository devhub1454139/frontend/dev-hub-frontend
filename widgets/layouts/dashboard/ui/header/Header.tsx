import classNames from 'classnames';
import Link from 'next/link';
import { Logo } from '@/shared/ui/logo/Logo';
import { AvatarDropdown } from '@/features/user/avatar-dropdown';
import { UserProfile } from '@/entities/user';
import cls from './header.module.scss';

export const Header = () => (
  <header className={classNames(cls.header)}>
    <Link href="/">
      <Logo />
    </Link>
    <AvatarDropdown
      className="hidden md:block"
      UserProfileSlot={
        <UserProfile
          name="Creezed"
          description="Product Designer"
          className="hidden md:flex"
          avatarSrc="https://lh3.googleusercontent.com/a/ACg8ocJbJhgDqw7lqJ7yW9HSRph0l9XF9xIL5gPpRUvDE_AQAN8=s288-c-no"
        />
      }
    />
  </header>
);
