import classNames from 'classnames';
import cls from './menu-item.module.scss';
import { MenuItem } from '../../../model/types';

export const MenuListItem = ({ label, IconComponent }: MenuItem) => (
  <div
    className={classNames(
      cls.item,
      'text-base dark:hover:bg-white dark:text-white dark:hover:text-black hover:bg-gray-400 w-full rounded-2xl gap-4 px-4 py-2 transition active:scale-95'
    )}
  >
    <IconComponent />
    <span>{label}</span>
  </div>
);
