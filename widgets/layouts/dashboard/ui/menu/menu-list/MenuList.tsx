import { MenuListItem } from '../menu-list-item';
import { MenuItem } from '../../../model/types';

interface MenuProps {
  items: MenuItem[];
}

export const MenuList = ({ items }: MenuProps) => (
  <nav className="flex flex-col gap-2 items-start">
    {items.map(({ label, IconComponent }) => (
      <MenuListItem key={label} label={label} IconComponent={IconComponent} />
    ))}
  </nav>
);
