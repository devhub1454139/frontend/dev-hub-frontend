import type { Config } from 'tailwindcss';
import { nextui } from '@nextui-org/react';

const config: Config = {
  content: [
    './app/**/*.tsx',
    './widgets/**/*.tsx',
    './features/**/*.tsx',
    './entities/**/*.tsx',
    './shared/**/*.tsx',
    './node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}'
  ],
  theme: {
    extend: {}
  },
  darkMode: 'class',
  plugins: [nextui()]
};

export default config;
