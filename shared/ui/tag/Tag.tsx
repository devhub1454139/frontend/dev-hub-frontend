import { PropsWithChildren } from 'react';
import classNames from 'classnames';

interface Props extends PropsWithChildren {
  className?: string;
}

export const Tag = ({ children, className }: Props) => (
  <div
    className={classNames('px-3 py-1 rounded-lg text-sm text-white', className)}
  >
    {children}
  </div>
);
