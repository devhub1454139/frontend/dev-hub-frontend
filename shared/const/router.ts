export const getRouteMain = (): string => '/';

export const getRouteProfile = (id: string): string => `/profile/${id}`;

export const getRouteRegistration = (): string => '/registration';

export const getRouteSignIn = (): string => '/sign-in';
